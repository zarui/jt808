package com.ltmonitor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
@Entity
@Table(name="Vehicle")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class DepartmentBak extends TenantEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DepId", unique = true, nullable = false)
	
	private int entityId;
	@Override
	public final int getEntityId() {
		return entityId;
	}
	@Override
	public final void setEntityId(int value) {
		entityId = value;
	}
	//上级部门
	private int parentId;

	//部门名称
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	
	

}
