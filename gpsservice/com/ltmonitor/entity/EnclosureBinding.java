package com.ltmonitor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * 围栏绑定类，
 * @author Administrator
 *
 */

@Entity
@Table(name="EnclosureBinding")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class EnclosureBinding extends TenantEntity{
	
	private int enclosureId;
	/**
	 * 绑定车辆
	 */
	private int vehicleId;
	/**
	 * 绑定部门
	 */
	private int depId;
	
	public EnclosureBinding()
	{
		
	}
	public EnclosureBinding(int _eId, int _vId)
	{
		enclosureId = _eId;
		vehicleId = _vId;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "bindId", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	
	public int getEnclosureId() {
		return enclosureId;
	}
	public void setEnclosureId(int enclosureId) {
		this.enclosureId = enclosureId;
	}
	public int getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}
	public int getDepId() {
		return depId;
	}
	public void setDepId(int depId) {
		this.depId = depId;
	}

}
