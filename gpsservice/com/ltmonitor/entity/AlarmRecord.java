﻿ package com.ltmonitor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

//停车报警记录表

@Entity
@Table(name="AlarmRecord")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class AlarmRecord extends TenantEntity
{
	//public static final String TYPE_WARN = "Warn"; //报警记录
	public static final String TYPE_STATE = "State"; //状态记录
	public static final String TYPE_PARKING = "Stop"; //停车记录
	public static final String TYPE_ONLINE = "GpsOnline"; //在线记录
	public static final String TYPE_OFFLINE = "GpsOffline"; //离线记录
	public static final String TYPE_OFFSET_ROUTE = "OffsetRoute"; //路线偏移
	public static final String TYPE_ON_ROUTE = "OnRoute"; //路线偏移
	public static final String TYPE_OVER_SPEED_ON_ROUTE = "OverSpeedOnRoute"; //分段限速
	public static final String TYPE_ARRIVE_NOT_ON_TIME = "ArriveNotOnTime"; //规定时间到达
	public static final String TYPE_LEAVE_NOT_ON_TIME = "LeaveNotOnTime"; //规定时间离开

    public static String TYPE_CROSS_BORDER = "CrossBorder"; //围栏报警
    public static String TYPE_IN_AREA = "InArea"; //进入区域
	public static final String STATUS_NEW = "New"; //开始状态
	public static final String STATUS_OLD = "Old"; //结束状态
	
	public static String ALARM_FROM_PLATFORM = "platform_alarm";//平台报警;
	public static String ALARM_FROM_TERM = "terminal_alarm";//终端报警;
	public static String ALARM_FROM_GOV = "gov_alarm";//政府平台报警;
	public static String STATE_FROM_TERM = "terminal_state";//终端状态变化报警;
	

	public AlarmRecord()
	{
		setCreateDate(new java.util.Date());
		setType(ALARM_FROM_TERM);
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "alarmId", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}

	private String plateNo;
	public final String getPlateNo()
	{
		return plateNo;
	}
	public final void setPlateNo(String value)
	{
		plateNo = value;
	}

	private java.util.Date startTime = new java.util.Date(0);
	public final java.util.Date getStartTime()
	{
		return startTime;
	}
	public final void setStartTime(java.util.Date value)
	{
		startTime = value;
	}

	private java.util.Date endTime;
	public final java.util.Date getEndTime()
	{
		return endTime;
	}
	public final void setEndTime(java.util.Date value)
	{
		endTime = value;
	}
	//开始的地点
	private String location;
	public final String getLocation()
	{
		return location;
	}
	public final void setLocation(String value)
	{
		location = value;
	}

	//最终的地点
	private String location1;
	public final String getLocation1()
	{
		return location1;
	}
	public final void setLocation1(String value)
	{
		location1 = value;
	}
	//行驶速度
	private double velocity;
	public final double getVelocity()
	{
		return velocity;
	}
	public final void setVelocity(double value)
	{
		velocity = value;
	}

	//时间间隔，以分钟为单位
	private double timeSpan;
	public final double getTimeSpan()
	{
		return timeSpan;
	}
	public final void setTimeSpan(double value)
	{
		timeSpan = value;
	}
	//记录的状态
	private String status;
	public final String getStatus()
	{
		return status;
	}
	public final void setStatus(String value)
	{
		status = value;
	}

	private double longitude;
	public final double getLongitude()
	{
		return longitude;
	}
	public final void setLongitude(double value)
	{
		longitude = value;
	}

	private double latitude;
	public final double getLatitude()
	{
		return latitude;
	}
	public final void setLatitude(double value)
	{
		latitude = value;
	}

	private double longitude1;
	public final double getLongitude1()
	{
		return longitude1;
	}
	public final void setLongitude1(double value)
	{
		longitude1 = value;
	}

	private double latitude1;
	public final double getLatitude1()
	{
		return latitude1;
	}
	public final void setLatitude1(double value)
	{
		latitude1 = value;
	}
	//司机
	private String driver;
	public final String getDriver()
	{
		return driver;
	}
	public final void setDriver(String value)
	{
		driver = value;
	}
	//> 0 代表此记录已经报过警，避免重复报警,默认是0
	private int processed;
	public final int getProcessed()
	{
		return processed;
	}
	public final void setProcessed(int value)
	{
		processed = value;
	}
	//记录类型
	private String type;
	public final String getType()
	{
		return type;
	}
	public final void setType(String value)
	{
		type = value;
	}
	//记录子类型
	private String childType;
	public final String getChildType()
	{
		return childType;
	}
	public final void setChildType(String value)
	{
		childType = value;
	}

	//地点类型
	private int station;
	public final int getStation()
	{
		return station;
	}
	public final void setStation(int value)
	{
		station = value;
	}
	//处理标志，可填写处理原因等
	private String flag;
	public final String getFlag()
	{
		return flag;
	}
	public final void setFlag(String value)
	{
		flag = value;
	}
	//视频文件名称
	private String videoFileName;
	public final String getVideoFileName()
	{
		return videoFileName;
	}
	public final void setVideoFileName(String value)
	{
		videoFileName = value;
	}

	//起始和终止的油量
	private double gas1;
	public final double getGas1()
	{
		return gas1;
	}
	public final void setGas1(double value)
	{
		gas1 = value;
	}

	private double gas2;
	public final double getGas2()
	{
		return gas2;
	}
	public final void setGas2(double value)
	{
		gas2 = value;
	}
	//起始和终止的里程
	private double mileage1;
	public final double getMileage1()
	{
		return mileage1;
	}
	public final void setMileage1(double value)
	{
		mileage1 = value;
	}

	private double mileage2;
	public final double getMileage2()
	{
		return mileage2;
	}
	public final void setMileage2(double value)
	{
		mileage2 = value;
	}


	public final int CompareTo(Object obj)
	{
		//throw new NotImplementedException();

		AlarmRecord vi = (AlarmRecord)obj;

		return getStartTime().compareTo(vi.getStartTime());


	}



}