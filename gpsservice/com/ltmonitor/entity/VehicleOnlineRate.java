package com.ltmonitor.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
/**
 * 单个车辆的上线率在某一个时段内的统计，统计最小粒度是天。不能按任意时间统计
 * @author DELL
 *
 */
@Entity
@Table(name="VehicleOnlineRate")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class VehicleOnlineRate extends TenantEntity{

	public static final int STATIC_BY_HOUR = 0; //按小时统计；
	public static final int STATIC_BY_DAY = 1;
	public static final int STATIC_MONTH = 2;
	public static final int STATIC_TIME_SPAN = 3; //按时间段统计
	
	public VehicleOnlineRate()
	{
		this.setCreateDate(new Date());
		this.intervalType = STATIC_BY_DAY;
		this.totalTime = 24 * 60;//24小时
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private int entityId;

	private String plateNo;
	//上线时间
	private double onlineTime;
	//离线时间
	private double offlineTime;
	//总时间
	private double totalTime;
	
	private double onlineRate;
	
	private int intervalType;
	//统计日期
	private Date staticDate;
	
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	
	public String getPlateNo() {
		return plateNo;
	}
	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public double getOnlineTime() {
		return onlineTime;
	}
	public void setOnlineTime(double onlineTime) {
		this.onlineTime = onlineTime;
	}

	public double getOfflineTime() {
		return offlineTime;
	}
	public void setOfflineTime(double offlineTime) {
		this.offlineTime = offlineTime;
	}

	public double getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(double totalTime) {
		this.totalTime = totalTime;
	}

	public Date getStaticDate() {
		return staticDate;
	}
	public void setStaticDate(Date staticDate) {
		this.staticDate = staticDate;
	}
	public int getIntervalType() {
		return intervalType;
	}
	public void setIntervalType(int intervalType) {
		this.intervalType = intervalType;
	}
	public double getOnlineRate() {
		return onlineRate;
	}
	public void setOnlineRate(double onlineRate) {
		this.onlineRate = onlineRate;
	}

}
