package com.ltmonitor.service;

import com.ltmonitor.entity.GnssData;
import com.ltmonitor.entity.TakePhotoModel;
import com.ltmonitor.entity.WarnData;
import com.ltmonitor.jt809.entity.VehicleRegisterInfo;
import com.ltmonitor.jt809.entity.DriverModel;


public interface ITransferService {

	/**
	 * 转发实时数据
	 * @param gnssData
	 */
	public abstract void UpExgMsgRealLocation(GnssData gnssData);

	public abstract boolean UpCtrlMsgMonitorVehicleAck(String plateNo,
			int plateColor, byte result);

	public abstract boolean UpCtrlMsgEmergencyMonitoringAck(String plateNo,
			int plateColor, byte result);

	public abstract boolean UpCtrlMsgTextInfoAck(String plateNo, int plateColor,
			int msgId, byte result);

	public abstract boolean UpCtrlMsgTakeTravelAck(String plateNo, int plateColor,
			byte cmdType, byte[] cmdData);

	public abstract boolean UpExgMsgReportTakeEWayBill(String plateNo,
			int plateColor, String eContent);

	public abstract boolean UpCtrlMsgTakePhotoAck(TakePhotoModel _photo);

	/**
	 * 上传车辆注册信息
	 * @param vm
	 * @return
	 */
	boolean UpExgMsgRegister(VehicleRegisterInfo vm);
	/**
	 * 主动上报驾驶员身份
	 * 
	 * @param dm
	 * @return
	 */
	boolean UpExgMsgReportDriverInfo(DriverModel dm);
	/**
	 * 上报报警信息
	 * @param wd
	 * @return
	 */
	boolean UpWarnMsgAdptInfo(WarnData wd);

}